## Package runnable jar:
```
sbt dist
```
## Extract zip to server 172.27.11.85 location /home/vinhdp4/api/
```
tar -vzxf api.tar.gz
```
## cd to /home/vinhdp4/api/ftel-big-data-api-1.0-SNAPSHOT/
```
cd /home/vinhdp4/api/ftel-big-data-api-1.0-SNAPSHOT/
```
## Create logs directory
```
mkdir logs
```
## Start api instance
```
nohup bin/ftel-big-data-api -Dconfig.file=conf/prod/application.conf > logs/server.log 2>&1 &
```
## Test API with token "jw-token" in request header using in local network:
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InByaXZhdGUyN0BmcHQuY29tLnZuIiwidXNlcklkIjoicHJpdmF0ZTI3In0.vRfk02ywbGSIqD18qp6lOi9WPGXwuvJq6nmxJB44WHU
```
## Using PostMan:
```
http://bigdata-api.fpt.vn/api/json/bras-detail?brasId=123&minutes=100
```

# Notes: stop running api before start new instance
```
ps -aux | grep ftel-big-data
kill -9 pid
```
