package utils

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}

import scala.util.{Failure, Success, Try}

/**
  * Created by vinhdp on 5/30/17.
  */
object DateTimeUtils {

    def isDate(date: String): Boolean = {

        Try {
            LocalDate.parse(date, DateTimeFormatter.ofPattern(Constants.DEFAULT_DATE_FORMAT))
        } match {
            case Success(d) => true
            case Failure(e) => {
                e.printStackTrace
                false
            }
        }
    }

    def main(args: Array[String]): Unit = {
        println(isDate("2015-05-01"))
    }
}
