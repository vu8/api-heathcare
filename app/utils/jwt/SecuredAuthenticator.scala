package utils.jwt

import javax.inject.Inject

import play.api.libs.json.Json
import play.api.mvc._
import services.UserService

import scala.concurrent.Future

/**
  * Created by vinhdp on 5/30/17.
  */
case class UserInfo(id: Int, firstName: String, lastName: String, email: String)    // using for load user info
case class User(email: String, userId: String)  // using for parse payload
case class UserRequest[A](userInfo: UserInfo, request: Request[A]) extends WrappedRequest(request)

class SecuredAuthenticator @Inject()(userService: UserService) extends Controller {
    implicit val formatUserDetails = Json.format[User]

    object JWTAuthentication extends ActionBuilder[UserRequest] {

        def invokeBlock[A](request: Request[A], block: (UserRequest[A]) => Future[Result]): Future[Result] = {
            val jwtToken = request.headers.get("jw-token").getOrElse("")

            if (JwtUtility.isValidToken(jwtToken)) {
                JwtUtility.decodePayload(jwtToken).fold {
                    Future.successful(Unauthorized("Invalid credential"))
                } { payload =>
                    val userCredentials = Json.parse(payload).validate[User].get
                    // Replace this block with data source
                    val maybeUserInfo = userService.getUser(userCredentials.email, userCredentials.userId)

                    maybeUserInfo.fold(Future.successful(Unauthorized("Invalid credential")))(userInfo => block(UserRequest(userInfo, request)))
                }
            } else {
                Future.successful(Unauthorized("Invalid credential"))
            }
        }
    }

}
