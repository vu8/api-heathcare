package utils.jwt

import authentikat.jwt.{JsonWebToken, JwtClaimsSet, JwtHeader}

/**
  * Created by vinhdp on 5/30/17.
  */
object JwtUtility {

    val JwtSecretKey = "%(Devd@EC>7h8Y-L"
    val JwtSecretAlgo = "HS256"

    def createToken(payload: String): String = {
        val header = JwtHeader(JwtSecretAlgo)
        val claimsSet = JwtClaimsSet(payload)

        JsonWebToken(header, claimsSet, JwtSecretKey)
    }

    def isValidToken(jwtToken: String): Boolean =
        JsonWebToken.validate(jwtToken, JwtSecretKey)

    def decodePayload(jwtToken: String): Option[String] =
        jwtToken match {
            case JsonWebToken(header, claimsSet, signature) => Option(claimsSet.asJsonString)
            case _                                          => None
        }

    def main(args: Array[String]): Unit = {
        println(createToken("""{"email":"private27@fpt.com.vn","userId":"private27"}"""))
    }
}
