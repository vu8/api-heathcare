package handlers

import play.api.http.HttpErrorHandler
import play.api.mvc.{RequestHeader, Result}
import play.api.mvc.Results._

import scala.concurrent.Future
import javax.inject._

/**
  * Created by vinhdp on 5/29/17.
  */
@Singleton
class ErrorHandler extends HttpErrorHandler {
    override def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
        Future.successful(
            Status(statusCode)("A client error occurred: " + message)
        )
    }

    override def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
        Future.successful(
            InternalServerError("A server error occurred: " + exception.getMessage)
        )
    }
}
