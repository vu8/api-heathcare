package services.noc

import javax.inject.{Inject, Singleton}

import daos.inf.DetectDisconectDAO
import models.{BrasDetail, InfDisconectOutlier}

import scala.concurrent.Future

/**
  * Created by hungdv on 23/08/2017.
  */

trait DetectDisconectService {

  def getOutlier(from: String,to: String): Future[Seq[InfDisconectOutlier]]

}

@Singleton
class DetectDisconectServiceImpl @Inject()(protected val detectDisconectDAO: DetectDisconectDAO)
  extends DetectDisconectService {

  override def getOutlier(from: String,to: String): Future[Seq[InfDisconectOutlier]] = {

    detectDisconectDAO.filter(from, to)
  }
}


