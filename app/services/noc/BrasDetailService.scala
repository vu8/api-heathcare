package services.noc

import javax.inject.{Inject, Singleton}

import daos.noc.BrasDetailDAO
import models.BrasDetail

import scala.concurrent.Future

/**
  * Created by vinhdp on 8/3/17.
  */
trait BrasDetailService {

    def filter(brasId: String, minutes: Int): Future[Seq[BrasDetail]]
    def filter(brasId: String, from: String, to: String): Future[Seq[BrasDetail]]
    def filterLabel(label: String, minutes: Int): Future[Seq[BrasDetail]]
    def filterLabel(label: String, from: String, to: String): Future[Seq[BrasDetail]]
}

@Singleton
class BrasDetailServiceImpl @Inject()(protected val brasDetailDAO: BrasDetailDAO)
  extends BrasDetailService {

    override def filter(brasId: String, minutes: Int): Future[Seq[BrasDetail]] = {

        brasDetailDAO.filter(brasId, minutes)
    }

    override def filter(brasId: String, from: String, to: String): Future[Seq[BrasDetail]] = {

        brasDetailDAO.filter(brasId, from, to)
    }

    override def filterLabel(label: String, minutes: Int): Future[Seq[BrasDetail]] = {

        brasDetailDAO.filterLabel(label, minutes)
    }

    override def filterLabel(label: String, from: String, to: String): Future[Seq[BrasDetail]] = {

        brasDetailDAO.filterLabel(label, from, to)
    }
}


