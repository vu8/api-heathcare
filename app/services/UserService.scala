package services

import javax.inject.{Inject, Singleton}

import daos.UserDAO
import utils.jwt.UserInfo

/**
  * Created by vinhdp on 5/30/17.
  */
trait UserService {
    def getUser(email: String, userId: String): Option[UserInfo]
}

@Singleton
class UserServiceImpl @Inject()(userDAO: UserDAO) extends UserService {
    override def getUser(email: String, userId: String): Option[UserInfo] = {

        userDAO.getUser(email, userId)
    }
}