package services

import javax.inject.{Inject, Singleton}

import daos.{ContractDAO, JsonContract}
import models.Contract

import scala.concurrent.Future

/**
  * Created by vinhdp on 5/29/17.
  */
trait ContractService {

    def insert(contract: Contract): Future[String]
    def insertAll(contracts: List[Contract]): Future[Seq[String]]
    def update(contract: Contract): Future[Int]
    def delete(contract: String): Future[Int]
    def getAll(): Future[List[Contract]]
    def getById(contract: String): Future[Option[Contract]]
    def filterStatus(status: String): Future[List[Contract]]
    def filter(date: String, status: Int): Future[Seq[JsonContract]]
}

@Singleton
class ContractServiceImpl @Inject()(protected val contractDAO: ContractDAO) extends ContractService {
    override def insert(contract: Contract): Future[String] = {
        contractDAO.insert(contract)
    }

    override def insertAll(contracts: List[Contract]): Future[Seq[String]] = {
        contractDAO.insertAll(contracts)
    }

    override def update(contract: Contract): Future[Int] = {
        contractDAO.update(contract)
    }

    override def delete(contract: String): Future[Int] = {
        contractDAO.delete(contract)
    }

    override def getAll(): Future[List[Contract]] = {
        contractDAO.getAll()
    }

    override def getById(contract: String): Future[Option[Contract]] = {
        contractDAO.getById(contract)
    }

    def filterStatus(status: String): Future[List[Contract]] = {

        status match {
            case "contact" => contractDAO.filterStatus(status)
            case _ => contractDAO.getAll()
        }
    }

    override def filter(date: String, status: Int): Future[Seq[JsonContract]] = {
        contractDAO.filter(date, status)
    }
}