package services.inf

import javax.inject.{Inject, Singleton}

import daos.bigdata.BigDataContractDAO
import daos.inf.InfContractDAO
import models.{InfraContractError, JsonInfraContractError}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits._


/**
  * Created by vinhdp on 6/20/17.
  */
trait InfContractService {

    def filter(date: String, force: String): Future[Seq[JsonInfraContractError]]
}

@Singleton
class InfContractServiceImpl @Inject()(protected val infContractDAO: InfContractDAO,
                                       protected val dataContractDAO: BigDataContractDAO)
  extends InfContractService {

    override def filter(date: String, force: String): Future[Seq[JsonInfraContractError]] = Future {
        // get all infra contract error
        val lstErrors = infContractDAO.filter(date)

        val inserts = Await.result(lstErrors, 30 seconds).groupBy(record => (record.contract, record.date)).mapValues{ values =>
            var nCpeError = 0
            var cpeSignin = 0
            var cpeLogoff = 0

            var nLostIpError = 0
            var lostIpSignin = 0
            var lostIpLogoff = 0

            values.map{ value =>
                if(value.error == "module/cpe error") {
                    nCpeError += value.nError
                    cpeSignin += value.signin
                    cpeLogoff += value.logoff
                } else {
                    nLostIpError = value.nError
                    lostIpSignin = value.signin
                    lostIpLogoff = value.logoff
                }
            }
            (nCpeError, cpeSignin, cpeLogoff, nLostIpError, lostIpSignin, lostIpLogoff)
        }.map{
            case (k, v) => JsonInfraContractError(k._1, k._2, v._1, v._2, v._3, v._4, v._5, v._6)
        }.toSeq

        if("yes" == force) {
            Await.result(dataContractDAO.delete(date), 30 seconds)
        }

        Await.result(dataContractDAO.insertAll(inserts), 30 seconds)

        inserts
    }
}
