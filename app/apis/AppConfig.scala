package apis

import com.typesafe.config.ConfigFactory

/**
  * Created by vinhdp on 6/21/17.
  */
object AppConfig {

    val appConfig = ConfigFactory.load
    val env = appConfig.getString("env")

    object Inf {
        val NUMBER_OF_ERROR = appConfig.getInt("api.infra.contract.error.nError")
    }
}
