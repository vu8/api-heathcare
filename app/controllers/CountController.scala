package controllers

import java.io.{BufferedWriter, File, FileWriter}
import java.sql.Date
import javax.inject._

import au.com.bytecode.opencsv.CSVWriter
import daos.EmployeeDAO
import models.Employee
import play.api.Logger
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Action, Controller}
import services.Counter

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._
import play.api.libs.json.Json._
import play.api.libs.json._

import scala.collection.JavaConversions._

/**
  * This controller demonstrates how to use dependency injection to
  * bind a component into a controller class. The class creates an
  * `Action` that shows an incrementing count to users. The [[Counter]]
  * object is injected by the Guice dependency injection system.
  */
@Singleton
class CountController @Inject()(counter: Counter, employeeDAO: EmployeeDAO, val messagesApi: MessagesApi)
    (implicit ec: ExecutionContext) extends Controller with I18nSupport {

    val logger = Logger(this.getClass())

    /**
      * Create an action that responds with the [[Counter]]'s current
      * count. The result is plain text. This `Action` is mapped to
      * `GET /count` requests by an entry in the `routes` config file.
      */

    def count = Action {

        val emp = Employee(Option(1), "Vinh 2", "Vinh Long", Option(Date.valueOf("2017-01-01")), Date.valueOf("2017-01-02"), Option("AD"))
        val result = Try(Await.result(employeeDAO.insert(emp), Duration.Inf)) match {
            case Success(i) => i
            case Failure(e) =>
                e.printStackTrace()
        }

        Ok("EmployeeID: " + result + "\n")
    }

    def index = Action.async { implicit request =>

        implicit val dateWrites = Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")
        implicit val employeeFormat = Json.format[Employee]

        employeeDAO.getAll().map( employee =>
            Ok(successResponse(Json.toJson(employee), Messages("emp.success.empList")))
        )
    }

    private def successResponse(data: JsValue, message: String) = {
        obj("status" -> "success", "data" -> data, "msg" -> message)
    }

    def read = Action {

        Ok.sendFile(new File("public/data/test.csv"))
          .withHeaders(
              CONTENT_TYPE -> "application/x-download",
              CONTENT_DISPOSITION -> "attachment; filename=test.csv"
          )
    }

    def write = Action.async {

        employeeDAO.getAll().map { res =>

            implicit val dateWrites = Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")
            implicit val employeeFormat = Json.format[Employee]

            val results = res.map(e => Array(
                e.id.getOrElse().toString,
                e.name,
                e.address,
                e.dob.getOrElse().toString,
                e.joiningDate.toString,
                e.designation.getOrElse().toString)
            )

            val employeeSchema=Array("name","age","dept")
            val employee1= Array("piyush","23","compute,rscience" + res.size)
            val employee2= Array("neel","24","computerscience 1")
            val employee3= Array("aayush","27","computerscience")
            val listOfRecords = List(employeeSchema,employee1,employee2,employee3)
            val outputFile = new BufferedWriter(new FileWriter("public/data/output.csv"))
            val csvWriter = new CSVWriter(outputFile)
            csvWriter.writeAll(results.toList)
            outputFile.close

            Ok(successResponse(Json.toJson(res), Messages("emp.success.empList")))
        }
    }

    def select = Action.async {

        implicit val dateWrites = Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")
        implicit val employeeFormat = Json.format[Employee]

        val futureEmployee = employeeDAO.getById(8)
        futureEmployee.map { employee =>

            Ok(successResponse(Json.toJson(employee), Messages("emp.success.empList")))
        }
    }

    // TODO: add param to force download
    def download = Action {

        val downloadFile = new File("public/data/output.csv")
        if(downloadFile.exists) {
            // file exist => check if modified
            println("File exist => download only")
        } else {
            // create download file
            println("File not exist => create & download")
            val results: List[Array[String]] = Try(Await.result(employeeDAO.getAll(), Duration.Inf)) match {
                case Success(employees) => {
                    employees.map(e => Array(
                            e.id.getOrElse().toString,
                            e.name,
                            e.address,
                            e.dob.getOrElse().toString,
                            e.joiningDate.toString,
                            e.designation.getOrElse().toString
                        )
                    )
                }
                case Failure(e) => {
                    e.printStackTrace()
                    List(Array())
                }
            }

            val outputFile = new BufferedWriter(new FileWriter("public/data/output.csv"))
            val csvWriter = new CSVWriter(outputFile)
            csvWriter.writeAll(results.toList)
            outputFile.close
        }

        // download the file
        Ok.sendFile(new File("public/data/predict_2017_5.csv"))
          .withHeaders(
              CONTENT_TYPE -> "application/x-download",
              CONTENT_DISPOSITION -> "attachment; filename=output.csv"
          )
    }
}
