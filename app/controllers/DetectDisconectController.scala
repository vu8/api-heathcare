package controllers

import java.sql.Timestamp
import java.util.{Calendar, Date}
import java.text.{DateFormat, SimpleDateFormat}
import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}

import models.InfDisconectOutlier
import play.api.Logger
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.json.Json.obj
import play.api.mvc.Controller
import services.noc.DetectDisconectService
import utils.jwt.SecuredAuthenticator

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by hungdv on 23/08/2017.
  */
@Singleton
class DetectDisconectController @Inject()(val messagesApi: MessagesApi, auth: SecuredAuthenticator, detectOutlierService: DetectDisconectService)
                                    (implicit ec: ExecutionContext) extends Controller with I18nSupport {

  val logger = Logger(this.getClass())

  def getOutlier(from: Option[String], to: Option[String]) = auth.JWTAuthentication.async{
    implicit request =>

      implicit val outlierFormat = Json.format[InfDisconectOutlier]

      if((from.isDefined && !to.isDefined) || (!from.isDefined && to.isDefined)){
        Future{
          BadRequest(failedResponse(Messages("outlierDisconnect.failed")))
        }
      }
      else if(from.isDefined && to.isDefined) {
        detectOutlierService.getOutlier(from.get, to.get).map {
          outlier =>
            Ok(successResponse(Json.toJson(outlier), Messages("bras.success.filterList")))
            //Ok(successResponse(Json.toJson(outlier), Messages("bras.success.filterList")))
        }
      }
      else{
/*           val dateFormat: DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
           //val date: Date = new Date()
          val date: Calendar = Calendar.getInstance()
          val time: Long = date.getTimeInMillis
          val before5mins = new Date(time - 6*60*1000)
          val _from = dateFormat.format(before5mins)
          val _to = dateFormat.format(date)*/

        val _from = Timestamp.valueOf(LocalDateTime.now().minusSeconds(6 * 60))
        val _to = Timestamp.valueOf(LocalDateTime.now())



          detectOutlierService.getOutlier(_from.toString,_to.toString).map{
            outlier =>
              Ok(successResponse(Json.toJson(outlier),Messages("outlierDisconect.success.filterList")))
          }
        }
      }
  private def successResponse(data: JsValue, message: String) = {
    obj("status" -> "success", "data" -> data, "msg" -> message)
  }

  private def failedResponse(message: String) = {
    obj("status" -> "failed", "msg" -> message)
  }
}