package controllers

import javax.inject.{Inject, Singleton}

import models.BrasDetail
import play.api.Logger
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.json.Json.obj
import play.api.mvc.Controller
import services.noc.BrasDetailService
import utils.jwt.SecuredAuthenticator

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by vinhdp on 8/3/17.
  */
@Singleton
class BrasDetailController @Inject()(val messagesApi: MessagesApi, auth: SecuredAuthenticator, brasDetailService: BrasDetailService)
                                    (implicit ec: ExecutionContext) extends Controller with I18nSupport {

    val logger = Logger(this.getClass())

    def filterBrasDetail(label: Option[String], brasId: Option[String], minutes: Option[Int],
                         from: Option[String], to: Option[String]) = auth.JWTAuthentication.async { implicit request =>

        implicit val brasFormat = Json.format[BrasDetail]

        if(!(label.isDefined || brasId.isDefined)) {
            Future{
                BadRequest(failedResponse(Messages("bras.failed.filterList")))
            }
        } else if(label.isDefined){
            if (from.isDefined && to.isDefined) {
                brasDetailService.filterLabel(label.get, from.get, to.get).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            } else if (minutes.isDefined) {
                brasDetailService.filterLabel(label.get, minutes.get).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            } else {
                brasDetailService.filterLabel(label.get, minutes.getOrElse(15)).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            }
        } else {
            if (from.isDefined && to.isDefined) {
                brasDetailService.filter(brasId.get, from.get, to.get).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            } else if (minutes.isDefined) {
                brasDetailService.filter(brasId.get, minutes.get).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            } else {
                brasDetailService.filter(brasId.get, minutes.getOrElse(15)).map {
                    brasDetails =>
                        Ok(successResponse(Json.toJson(brasDetails), Messages("bras.success.filterList")))
                }
            }
        }
    }

    private def successResponse(data: JsValue, message: String) = {
        obj("status" -> "success", "data" -> data, "msg" -> message)
    }

    private def failedResponse(message: String) = {
        obj("status" -> "failed", "msg" -> message)
    }
}
