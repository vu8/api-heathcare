package controllers

import java.io.{BufferedWriter, File, FileWriter}
import javax.inject.{Inject, Singleton}

import au.com.bytecode.opencsv.CSVWriter
import daos.{ContractDAO, JsonContract}
import models.{Contract, JsonInfraContractError}
import play.api.Logger
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.json.Json.obj
import play.api.mvc.{Action, Controller}
import services.ContractService
import services.inf.InfContractService
import utils.DateTimeUtils
import utils.jwt.SecuredAuthenticator

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.collection.JavaConversions._

/**
  * Created by vinhdp on 5/29/17.
  */
@Singleton
class ContractController @Inject()(contractService: ContractService,
                                   infContractService: InfContractService,
                                   val messagesApi: MessagesApi,
                                   auth: SecuredAuthenticator)
                                  (implicit ec: ExecutionContext) extends Controller with I18nSupport {

    val logger = Logger(this.getClass())

    def contractJson(date: String, status: Option[String]) = Action.async { implicit request =>

        implicit val contractFormat = Json.format[Contract]
        contractService.filterStatus(status.getOrElse("")).map( contract =>
            Ok(successResponse(Json.toJson(contract), Messages("contract.success.contractList")))
        )
    }

    def contractCsv(date: String, status: Option[String]) = Action {

        val downloadFile = new File(s"public/data/predict_$date.csv")
        if(!downloadFile.exists) {
            val results: List[Array[String]] = Try(Await.result(contractService.filterStatus(status.getOrElse("")), Duration.Inf)) match {
                case Success(contracts) => {
                    contracts.map(c => Array(
                            c.contract,
                            c.typeCustomer,
                            c.priorityGroup.toString,
                            c.lifetimeGroup,
                            c.connectGroup,
                            c.needGroup,
                            c.chargeGroup,
                            c.checklistGroup,
                            c.checklistAmount.toString,
                            c.infError.toString,
                            c.daysInfError.toString,
                            c.region,
                            c.province,
                            c.feedback.getOrElse().toString,
                            c.csStatus.getOrElse().toString
                        )
                    )
                }
                case Failure(e) => {
                    e.printStackTrace()
                    List(Array())
                }
            }

            val outputFile = new BufferedWriter(new FileWriter(s"public/data/predict_$date.csv"))
            val csvWriter = new CSVWriter(outputFile)
            csvWriter.writeAll(results.toList)
            outputFile.close
        }

        // download the file
        Ok.sendFile(new File(s"public/data/predict_$date.csv"))
          .withHeaders(
              CONTENT_TYPE -> "application/x-download",
              CONTENT_DISPOSITION -> s"attachment; filename=predict_$date.csv"
          )
    }

    private def successResponse(data: JsValue, message: String) = {
        obj("status" -> "success", "data" -> data, "msg" -> message)
    }

    private def failedResponse(message: String) = {
        obj("status" -> "failed", "msg" -> message)
    }

    def contract(date: String, status: Option[Int]) = auth.JWTAuthentication.async { implicit request =>

        if(DateTimeUtils.isDate(date)) {

            implicit val contractFormat = Json.format[JsonContract]
            contractService.filter(date, status.getOrElse(-1)).map { contracts =>

                Ok(successResponse(Json.toJson(contracts), Messages("contract.success.contractList")))
            }
        } else {
            Future{
                BadRequest(failedResponse(Messages("contract.failed.contractList")))
            }
        }
    }

    def authen = auth.JWTAuthentication { implicit request =>

        Ok(s"Hello ${request.userInfo.firstName} ${request.userInfo.lastName}")
    }

    def getInfraContractError(date: String, force: Option[String]) = auth.JWTAuthentication.async { implicit request =>

        if(DateTimeUtils.isDate(date)) {

            implicit val contractFormat = Json.format[JsonInfraContractError]
            infContractService.filter(date, force.getOrElse("no")).map { contracts =>

                Ok(successResponse(Json.toJson(contracts), Messages("contract.success.contractList")))
            }
        } else {
            Future{
                BadRequest(failedResponse(Messages("contract.failed.contractList")))
            }
        }
    }
}
