package controllers

import javax.inject.Inject

import models.{InfraContractError, JsonInfraContractError}
import play.api.Logger
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.json.Json.obj
import play.api.mvc.{Action, Controller}
import services.Counter
import services.inf.InfContractService
import utils.DateTimeUtils

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by vinhdp on 6/20/17.
  */
class TestController @Inject()(counter: Counter, contractService: InfContractService, val messagesApi: MessagesApi)
                              (implicit ec: ExecutionContext) extends Controller with I18nSupport {

    val logger = Logger(this.getClass())

    def getInfraContractError(date: String, force: Option[String]) = Action.async {

        if(DateTimeUtils.isDate(date)) {

            implicit val contractFormat = Json.format[JsonInfraContractError]
            contractService.filter(date, force.getOrElse("no")).map { contracts =>

                Ok(successResponse(Json.toJson(contracts), Messages("contract.success.contractList")))
            }
        } else {
            Future{
                BadRequest(failedResponse(Messages("contract.failed.contractList")))
            }
        }
    }

    private def successResponse(data: JsValue, message: String) = {
        obj("status" -> "success", "data" -> data, "msg" -> message)
    }

    private def failedResponse(message: String) = {
        obj("status" -> "failed", "msg" -> message)
    }
}
