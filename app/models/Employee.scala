package models

import java.sql.Date

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * Created by vinhdp on 27/05/17.
  */
case class Employee(id: Option[Long] = None, name: String, address: String, dob: Option[Date], joiningDate: Date, designation: Option[String])

trait EmployeeTable {
    self: HasDatabaseConfigProvider[JdbcProfile] =>

    import slick.jdbc.PostgresProfile.api._

    lazy protected val empTableQuery = TableQuery[EmployeeTable]
    lazy protected val empTableQueryInc = empTableQuery returning empTableQuery.map(_.id)

    class EmployeeTable(tag: Tag) extends Table[Employee](tag, Some("kafka"), "employee") {

        //implicit val dateColumnType = MappedColumnType.base[Date, Long](d => d.getTime, d => new Date(d))

        def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)

        def name: Rep[String] = column[String]("name")

        def address: Rep[String] = column[String]("address")

        def dob: Rep[Date] = column[Date]("date_of_birth")

        def joiningDate: Rep[Date] = column[Date]("joining_date")

        def designation: Rep[String] = column[String]("designation")

        def * = (id.?, name, address, dob.?, joiningDate, designation.?) <> (Employee.tupled, Employee.unapply _)
    }
}