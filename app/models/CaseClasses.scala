package models

/**
  * Created by vinhdp on 6/20/17.
  */
case class InfraContractError(contract: String, date: String, error: String, nError: Int, signin: Int, logoff: Int)
case class JsonInfraContractError(contract: String, date: String, nCpeError: Int, cpeSignin: Int, cpeLogoff: Int, nLostIpError: Int, lostIpSignin: Int, lostIpLogoff: Int)
case class BrasDetail(dateTime: String, label: String, brasId: String, activeUser: Int, signin: Int, logoff: Int, cpeError: Int,
                      lostIpError: Int, critKibana: Int, infoKibana: Int, critOpsView: Int, okOpsView: Int, warnOpsView: Int, unknownOpsView: Int)