package models

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * Created by vinhdp on 5/29/17.
  */
case class Contract(contract: String, typeCustomer: String, priorityGroup: Int, lifetimeGroup: String,
                    connectGroup: String, needGroup: String, chargeGroup: String, checklistGroup: String,
                    checklistAmount: Int, infError: Int, daysInfError: Int, region: String, province: String,
                    feedback: Option[String], csStatus: Option[String])

trait ContractTable {
    self: HasDatabaseConfigProvider[JdbcProfile] =>

    import slick.jdbc.PostgresProfile.api._

    lazy protected val contractTableQuery = TableQuery[ContractTable]
    lazy protected val contractTableQueryInc = contractTableQuery returning contractTableQuery.map(_.contract)

    class ContractTable(tag: Tag) extends Table[Contract](tag, "api_contract") {

        def contract: Rep[String] = column[String]("contract", O.PrimaryKey)
        def typeCustomer: Rep[String] = column[String]("type_customer")
        def priorityGroup: Rep[Int] = column[Int]("priority_group")
        def lifetimeGroup: Rep[String] = column[String]("lifetime_group")
        def connectGroup: Rep[String] = column[String]("connect_group")
        def needGroup: Rep[String] = column[String]("need_group")
        def chargeGroup: Rep[String] = column[String]("charge_group")
        def checklistGroup: Rep[String] = column[String]("checklist_group")
        def checklistAmount: Rep[Int] = column[Int]("checklist_amount")
        def infError: Rep[Int] = column[Int]("inf_error")
        def daysInfError: Rep[Int] = column[Int]("days_inf_error")
        def region: Rep[String] = column[String]("region")
        def province: Rep[String] = column[String]("province")
        def feedback: Rep[String] = column[String]("feedback")
        def csStatus: Rep[String] = column[String]("cs_status")

        def * = (contract, typeCustomer, priorityGroup, lifetimeGroup, connectGroup, needGroup, chargeGroup,
          checklistGroup, checklistAmount, infError, daysInfError, region, province, feedback.?, csStatus.?) <> (Contract.tupled, Contract.unapply _)
    }
}