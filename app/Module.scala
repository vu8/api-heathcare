import java.time.Clock

import com.google.inject.AbstractModule
import daos._
import daos.bigdata.{BigDataContractDAO, BigDataContractDAOImpl}
import daos.inf.{DetecDisconnectDAOImpl, DetectDisconectDAO, InfContractDAO, InfContractDAOImpl}
import daos.noc.{BrasDetailDAO, BrasDetailDAOImpl}
import services._
import services.inf.{InfContractService, InfContractServiceImpl}
import services.noc.{BrasDetailService, BrasDetailServiceImpl, DetectDisconectService, DetectDisconectServiceImpl}

/**
  * Created by vinhdp on 26/05/17.
  *
  * This class is a Guice module that tells Guice how to bind several
  * different types. This Guice module is created when the Play
  * application starts.

  * Play will automatically use any class called `Module` that is in
  * the root package. You can create modules in other locations by
  * adding `play.modules.enabled` settings to the `application.conf`
  * configuration file.
  */
class Module extends AbstractModule {
    override def configure(): Unit = {
        // Use the system clock as the default implementation of Clock
        bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)
        // Set AtomicCounter as the implementation for Counter.
        bind(classOf[Counter]).to(classOf[AtomicCounter])
        // Set EmployeeDAOImp as the implement for EmployeeDAO
        bind(classOf[EmployeeDAO]).to(classOf[EmployeeDAOImpl])
        bind(classOf[ContractDAO]).to(classOf[ContractDAOImpl])
        bind(classOf[UserDAO]).to(classOf[UserDAOImpl])

        bind(classOf[InfContractDAO]).to(classOf[InfContractDAOImpl])
        bind(classOf[BigDataContractDAO]).to(classOf[BigDataContractDAOImpl])
        bind(classOf[BrasDetailDAO]).to(classOf[BrasDetailDAOImpl])

        bind(classOf[ContractService]).to(classOf[ContractServiceImpl])
        bind(classOf[UserService]).to(classOf[UserServiceImpl])

        bind(classOf[InfContractService]).to(classOf[InfContractServiceImpl])
        bind(classOf[BrasDetailService]).to(classOf[BrasDetailServiceImpl])

        bind(classOf[DetectDisconectService]).to(classOf[DetectDisconectServiceImpl])
        bind(classOf[DetectDisconectDAO]).to(classOf[DetecDisconnectDAOImpl])

    }
}
