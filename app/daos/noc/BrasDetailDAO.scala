package daos.noc

import java.sql.Timestamp
import java.time.{LocalDateTime}
import javax.inject.Inject

import models.BrasDetail
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.db.NamedDatabase
import slick.jdbc.{GetResult, JdbcProfile}

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._

/**
  * Created by vinhdp on 8/3/17.
  */
trait BrasDetailDAO extends HasDatabaseConfigProvider[JdbcProfile] {

    def filter(brasId: String, minutes: Int): Future[Seq[BrasDetail]]
    def filter(brasId: String, from: String, to: String): Future[Seq[BrasDetail]]
    def filterLabel(label: String, minutes: Int): Future[Seq[BrasDetail]]
    def filterLabel(label: String, from: String, to: String): Future[Seq[BrasDetail]]
}

class BrasDetailDAOImpl @Inject()(@NamedDatabase("noc") val dbConfigProvider: DatabaseConfigProvider)
  extends BrasDetailDAO {

    override def filter(brasId: String, minutes: Int): Future[Seq[BrasDetail]] = {

        implicit val getUserResult = GetResult(r => BrasDetail(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

        val time = Timestamp.valueOf(LocalDateTime.now().minusSeconds(minutes * 60))

        db.run {
            val sql =
                sql"""select date_time, label, bras_id, active_user, signin_total_count, logoff_total_count, cpe_error, lostip_error, crit_kibana,
                      info_kibana, crit_opsview, ok_opsview, warn_opsview, unknown_opsview
                     from dwh_radius_bras_detail
                     where bras_id = ${brasId} and date_time > ${time}
                  """
            sql.as[BrasDetail]
        }
    }

    override def filter(brasId: String, from: String, to: String): Future[Seq[BrasDetail]] = {

        implicit val getUserResult = GetResult(r => BrasDetail(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

        db.run {
            val sql =
                sql"""select date_time, label, bras_id, active_user, signin_total_count, logoff_total_count, cpe_error, lostip_error, crit_kibana,
                      info_kibana, crit_opsview, ok_opsview, warn_opsview, unknown_opsview
                     from dwh_radius_bras_detail
                     where bras_id = ${brasId} and date_time between ${from}::timestamp and ${to}::timestamp
                  """
            sql.as[BrasDetail]
        }
    }

    override def filterLabel(label: String, minutes: Int): Future[Seq[BrasDetail]] = {

        implicit val getUserResult = GetResult(r => BrasDetail(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

        val time = Timestamp.valueOf(LocalDateTime.now().minusSeconds(minutes * 60))

        db.run {
            val sql =
                sql"""select date_time, label, bras_id, active_user, signin_total_count, logoff_total_count, cpe_error, lostip_error, crit_kibana,
                      info_kibana, crit_opsview, ok_opsview, warn_opsview, unknown_opsview
                     from dwh_radius_bras_detail
                     where label = ${label} and date_time > ${time}
                  """
            sql.as[BrasDetail]
        }
    }

    override def filterLabel(label: String, from: String, to: String): Future[Seq[BrasDetail]] = {

        implicit val getUserResult = GetResult(r => BrasDetail(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

        db.run {
            val sql =
                sql"""select date_time, label, bras_id, active_user, signin_total_count, logoff_total_count, cpe_error, lostip_error, crit_kibana,
                      info_kibana, crit_opsview, ok_opsview, warn_opsview, unknown_opsview
                     from dwh_radius_bras_detail
                     where label = ${label} and date_time between ${from}::timestamp and ${to}::timestamp
                  """
            sql.as[BrasDetail]
        }
    }
}
