package daos.inf

import javax.inject.Inject

import apis.AppConfig
import models.InfraContractError
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.db.NamedDatabase
import slick.jdbc.{GetResult, JdbcProfile}

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._

/**
  * Created by vinhdp on 6/19/17.
  */
trait  InfContractDAO extends HasDatabaseConfigProvider[JdbcProfile] {

    def filter(date: String): Future[Seq[InfraContractError]]
}

class InfContractDAOImpl @Inject()(@NamedDatabase("inf") val dbConfigProvider: DatabaseConfigProvider)
  extends InfContractDAO {

    override def filter(date: String): Future[Seq[InfraContractError]] = {

        implicit val getUserResult = GetResult(r => InfraContractError(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

        db.run {
            val sql =
                sql"""select contract, date, error, sum(n_error), sum(signin), sum(logoff)
                     from detail
                     group by contract, date, error
                     having date = ${date}::date and sum(n_error) > ${AppConfig.Inf.NUMBER_OF_ERROR}
                     and contract is not null
                  """
            sql.as[InfraContractError]
        }
    }
}