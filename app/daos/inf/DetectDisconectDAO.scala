package daos.inf

import models.InfDisconectOutlier
import java.sql.Timestamp
import java.time.{LocalDateTime}
import javax.inject.Inject

import models.BrasDetail
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.db.NamedDatabase
import slick.jdbc.{GetResult, JdbcProfile}

import scala.concurrent.Future
import slick.jdbc.PostgresProfile.api._

/**
  * Created by hungdv on 23/08/2017.
  */
trait DetectDisconectDAO extends HasDatabaseConfigProvider[JdbcProfile]{
  def filter(from: String,to: String): Future[Seq[InfDisconectOutlier]]
}

class DetecDisconnectDAOImpl @Inject()(@NamedDatabase("noc") val dbConfigProvider: DatabaseConfigProvider)
extends DetectDisconectDAO{
  override def filter(from: String,to: String): Future[Seq[InfDisconectOutlier]] = {

    implicit val getUserResult = GetResult(r => InfDisconectOutlier(r.<<, r.<<,r.<<))

    db.run {
      val sql = sql""" select index, erro_count_by_index, server_time
               from disconect_pattern
               where server_time between ${from}::timestamp and ${to}::timestamp
           """
      sql.as[InfDisconectOutlier]
    }
  }
}