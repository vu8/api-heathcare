package daos

import utils.jwt.{UserInfo}
import javax.inject.{Singleton}

/**
  * Created by vinhdp on 5/30/17.
  */
trait UserDAO {
    def getUser(email: String, userId: String): Option[UserInfo]
}

@Singleton
class UserDAOImpl extends UserDAO {
    override def getUser(email: String, userId: String): Option[UserInfo] = {

        if (email == "private27@fpt.com.vn" && userId == "private27") {
            Some(UserInfo(1, "Private", "User", email))
        } else {
            None
        }
    }
}
