package daos.bigdata

import javax.inject.Inject

import models.{InfraContractError, JsonInfraContractError}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.db.NamedDatabase
import slick.jdbc.{JdbcProfile}
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Future

/**
  * Created by vinhdp on 6/19/17.
  */
trait BigDataContractDAO extends HasDatabaseConfigProvider[JdbcProfile] {

    def insertAll(contractErrors: Seq[JsonInfraContractError], date: String, force: String): Future[Unit]
    def insertAll(contractErrors: Seq[JsonInfraContractError]): Future[Unit]
    def delete(date: String): Future[Int]
}

class BigDataContractDAOImpl @Inject()(val dbConfigProvider: DatabaseConfigProvider)
  extends BigDataContractDAO {

    import dbConfig.profile.api._

    override def insertAll(contractErrors: Seq[JsonInfraContractError]): Future[Unit] = Future {

        val batchSql =
            s"""insert into infra_contract_error values (?, ?::date, ?, ?, ?, ?, ?, ?)
               |on conflict do nothing
             """.stripMargin
        def batchInsert(contractErrors: Seq[JsonInfraContractError])(implicit session: Session) = {
            val pstmt = session.conn.prepareStatement(batchSql)

            contractErrors.map { contract =>
                pstmt.setString(1, contract.contract)
                pstmt.setString(2, contract.date)
                pstmt.setInt(3, contract.nCpeError)
                pstmt.setInt(4, contract.cpeSignin)
                pstmt.setInt(5, contract.cpeLogoff)
                pstmt.setInt(6, contract.nLostIpError)
                pstmt.setInt(7, contract.lostIpSignin)
                pstmt.setInt(8, contract.lostIpLogoff)

                pstmt.addBatch()
            }

            pstmt.executeBatch()
        }

        val session = db.createSession()
        batchInsert(contractErrors)(session)
        session.close()
    }

    override def delete(date: String): Future[Int] = {

        db.run(sqlu"""delete from infra_contract_error where date = ${date}::date""")
    }

    override def insertAll(contractErrors: Seq[JsonInfraContractError], date: String, force: String): Future[Unit] = {

        def insert(c: JsonInfraContractError) = s"insert into infra_contract_error values (${c.contract}, ${c.date}::date," +
          s" ${c.nCpeError}, ${c.cpeSignin}, ${c.cpeLogoff}, ${c.nLostIpError}, ${c.lostIpSignin}, ${c.lostIpLogoff})"
        val inserts: Seq[String] = contractErrors.map(insert(_))
        //DBIO.seq(inserts)

        val query: DBIOAction[Unit, NoStream, Effect] = for {
            _ <- sqlu"""delete from infra_contract_error where date = ${date}::date"""
        } yield ()

        db.run(query.transactionally)
    }
}