package daos

import javax.inject.{Inject, Singleton}

import com.google.inject.ImplementedBy

import scala.concurrent.Future
import models.{Employee, EmployeeTable}
import slick.jdbc.JdbcProfile
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}

/**
  * Created by vinhdp on 27/05/17.
  */
trait EmployeeDAO extends EmployeeTable with HasDatabaseConfigProvider[JdbcProfile] {

    def insert(employee: Employee): Future[Long]
    def insertAll(employees: List[Employee]): Future[Seq[Long]]
    def update(employee: Employee): Future[Int]
    def delete(id: Long): Future[Int]
    def getAll(): Future[List[Employee]]
    def getById(empId: Long): Future[Option[Employee]]
}

@Singleton
class EmployeeDAOImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends EmployeeDAO {

    import slick.jdbc.PostgresProfile.api._

    override def insert(employee: Employee): Future[Long] = db.run {
        empTableQueryInc += employee
    }

    override def insertAll(employees: List[Employee]): Future[Seq[Long]] = db.run {
        empTableQueryInc ++= employees
    }

    override def update(employee: Employee): Future[Int] = db.run {
        empTableQuery.filter(_.id === employee.id).update(employee)
    }

    override def delete(id: Long): Future[Int] = db.run {
        empTableQuery.filter(_.id === id).delete
    }

    override def getAll(): Future[List[Employee]] = db.run {
        empTableQuery.to[List].result
    }

    override def getById(empId: Long): Future[Option[Employee]] = db.run {
        empTableQuery.filter(_.id === empId).result.headOption
    }

    def ddl = empTableQuery.schema
}