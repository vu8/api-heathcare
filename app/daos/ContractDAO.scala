package daos

import javax.inject.{Inject, Singleton}

import models.{Contract, ContractTable}
import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.{GetResult, JdbcProfile}

import scala.concurrent.Future

/**
  * Created by vinhdp on 5/29/17.
  */
trait ContractDAO extends ContractTable with HasDatabaseConfigProvider[JdbcProfile] {

    def insert(contract: Contract): Future[String]
    def insertAll(contracts: List[Contract]): Future[Seq[String]]
    def update(contract: Contract): Future[Int]
    def delete(contract: String): Future[Int]
    def getAll(): Future[List[Contract]]
    def getById(contract: String): Future[Option[Contract]]
    def filterStatus(status: String): Future[List[Contract]]
    def filter(date: String, status: Int): Future[Seq[JsonContract]]
}

case class JsonContract(contract: String, typeCustomer: String, priorityGroup: Int, lifetimeGroup: String,
    connectGroup: String, needGroup: String, chargeGroup: String, checklistGroup: String,
    checklistAmount: Int, infError: Int, daysInfError: Int, region: String, province: String)

@Singleton
class ContractDAOImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends ContractDAO {

    import slick.jdbc.PostgresProfile.api._
    val logger = Logger(this.getClass())

    override def insert(contract: Contract): Future[String] = db.run {
        contractTableQueryInc += contract
    }

    override def insertAll(contracts: List[Contract]): Future[Seq[String]] = db.run {
        contractTableQueryInc ++= contracts
    }

    override def update(contract: Contract): Future[Int] = db.run {
        contractTableQuery.filter(_.contract === contract.contract).update(contract)
    }

    override def delete(contract: String): Future[Int] = db.run {
        contractTableQuery.filter(_.contract === contract).delete
    }

    override def getAll(): Future[List[Contract]] = db.run {
        contractTableQuery.to[List].result
    }

    override def getById(contract: String): Future[Option[Contract]] = db.run {
        contractTableQuery.filter(_.contract === contract).result.headOption
    }

    def filterStatus(status: String): Future[List[Contract]] = db.run {
        contractTableQuery.filter(_.csStatus === status).to[List].result
    }

    def filter(date: String, status: Int): Future[Seq[JsonContract]] = {

        implicit val getUserResult = GetResult(r =>
            JsonContract(r.nextString, r.nextString, r.nextInt, r.nextString, r.nextString, r.nextString, r.nextString,
                r.nextString, r.nextInt, r.nextInt, r.nextInt, r.nextString , r.nextString))

        val r = db.run {
            /*sql"""select contract, type_customer, priority_group, lifetime_group, connect_group, need_group, charge_group,
              checklist_group, checklist_amount, inf_error, days_inf_error, region, province
              from api_contract"""
              .as[JsonContract]*/

            val plain = sql"""SELECT * FROM fe_get_api_contract_by_predict(${date}, ${status})"""

            plain.as[JsonContract]
        }
        r
    }

    def ddl = contractTableQuery.schema
}