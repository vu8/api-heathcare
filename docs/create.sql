create table detail (
  host varchar(30),
  module varchar(30),
  index varchar(30),
  error varchar(30),
  date date,
  time integer,
  n_error integer,
  province varchar(30),
  month date,
  contract varchar(30),
  last_signin varchar(30),
  last_logoff varchar(30),
  last_reject varchar(30),
  signin integer,
  logoff integer,
  reject integer,
  constraint detail_date primary key (date)
);

insert into detail values ('VPCP02201GC57', 7, 4, 'module/cpe error', '2017-06-18', 23, 3, 'VPC', '2017-06-01', 'VPFD00453', '23:55', '23:53', 'N/A', 8, 8, 0)

create table infra_contract_error (
  contract varchar(30),
  date date,
  n_cpe_error integer,
  cpe_signin integer,
  cpe_logoff integer,
  n_lost_ip_error integer,
  lost_ip_signin integer,
  lost_ip_logoff integer,
  primary key (contract, date)
);
insert into detail values ('VPCP02201GC67', 7, 4, 'module/cpe error', '2017-06-18', 23, 3, 'VPC', '2017-06-01', 'VPFD00454', '23:55', '23:53', 'N/A', 8, 8, 0);
insert into detail values ('VPCP02201GC67', 7, 4, 'module/cpe error', '2017-06-18', 23, 3, 'VPC', '2017-06-01', 'VPFD00453', '23:55', '23:53', 'N/A', 8, 8, 0);