name := "ftel-big-data-api"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
PlayKeys.devSettings := Seq("play.server.http.port" -> "9001")

scalaVersion := "2.11.8"

resolvers += "typesafe" at "http://repo.typesafe.com/typesafe/releases/"

// libraryDependencies += jdbc
libraryDependencies += cache
libraryDependencies += ws
libraryDependencies ++= Seq(
    "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test,
    "com.typesafe.play" %% "play-slick" % "2.1.0",
    "com.typesafe.play" %% "play-slick-evolutions" % "2.1.0",
    "org.postgresql" % "postgresql" % "42.1.1",
    "org.slf4j" % "slf4j-nop" % "1.7.25",
    "au.com.bytecode" % "opencsv" % "2.4",
    "com.chuusai" %% "shapeless" % "2.3.1",
    "io.underscore" %% "slickless" % "0.3.2",
    "com.jason-goodwin" % "authentikat-jwt_2.11" % "0.4.5"
)